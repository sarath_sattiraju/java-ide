import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
class DataBase extends Frame implements ActionListener{
  Button add1,del,search,add2;
  String str="";
  final Frame f=new Frame("ADD DETAILS");
  TextField rno;
  HashMap<String,String> hm=new HashMap<String,String>();
  GridBagConstraints c=new GridBagConstraints();
  TextField t1[]=new TextField[4];
  Label l1[]=new Label[4];
  Panel p[]=new Panel[5];
  Label l;
    DataBase(){
      super("Student Database");
      setLayout(new FlowLayout());
      setSize(500,100);
      setVisible(true);
      addWindowListener( new WindowAdapter() {
      		public void windowClosing(WindowEvent we)
      			{
      				 System.exit(0);
     			 }
    		 });
      rno=new TextField(20);
      add(rno);
      rno.addActionListener(this);

      add1=new Button("ADD");
      add(add1);
      add1.addActionListener(this);
      del=new Button("DELETE");
      add(del);
      del.addActionListener(this);
      search=new Button("SEARCH");
      add(search);
      search.addActionListener(this);
    }
    void create(int x){
      if(x==0){
        if((str=rno.getText().toString())!=""){
          f.setSize(400,400);
          f.addWindowListener( new WindowAdapter() {
                      public void windowClosing(WindowEvent we)
                        {
                           f.dispose();
                       }
                     });
          f.setLayout(new GridBagLayout());
          f.setVisible(true);
          for(int i=0;i<5;i++)
          p[i]=new Panel();
          p[0].add(new Label("Registered Number: "));
          p[0].add(new Label(str));
          c.gridx=0;
          c.gridy=0;
          f.add(p[0],c);

          t1[0]=new TextField(15);
          l1[0]=new Label("Name: ");
          p[1].add(l1[0]);
          p[1].add(t1[0]);
          t1[0].addActionListener(this);
          c.gridx=0;
          c.gridy=1;
          f.add(p[1],c);

          l1[1]=new Label("Year:    ");
          t1[1]=new TextField(15);
          p[2].add(l1[1]);
          p[2].add(t1[1]);
          t1[1].addActionListener(this);
          c.gridx=0;
          c.gridy=2;
          f.add(p[2],c);

          l1[2]=new Label("Branch:");
          t1[2]=new TextField(15);
          p[3].add(l1[2]);
          p[3].add(t1[2]);
          t1[2].addActionListener(this);
          c.gridx=0;
          c.gridy=3;
          f.add(p[3],c);

          l1[3]=new Label("CGPA: ");
          t1[3]=new TextField(15);
          p[4].add(l1[3]);
          p[4].add(t1[3]);
          t1[3].addActionListener(this);
          c.gridx=0;
          c.gridy=4;
          f.add(p[4],c);

          add2=new Button("ADD");
          add2.addActionListener(this);
          c.gridx=0;
          c.gridy=5;
          f.add(add2,c);
        }
    }
    if(x==1){
      f.removeAll();
      f.dispose();
    }
  }
    public void actionPerformed(ActionEvent e){
      if(e.getSource()==add1){
        create(0);
      }
      if(e.getSource()==add2){
        try{
        create(1);  
        createFile();
      }
      catch(IOException e1){}
    }
      if(e.getSource()==del){
	str=rno.getText().toString();
        if(hm.containsKey(str)==true){
          deleteFile();
        }
        else{
          final Frame f1=new Frame();
          f1.setLayout(new FlowLayout());
          f1.setSize(100,100);
          f1.setVisible(true);
          f1.add(new Label("NOT FOUND"));
          f1.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent we){
              f1.dispose();
            }
          });
      }
    }
      if(e.getSource()==search){
	str=rno.getText().toString();
        if(hm.containsKey(str)==true){
          try{
          displayFile();
        }
        catch(IOException e1){}
      }
        else{
          final Frame f2=new Frame();
          f2.setLayout(new FlowLayout());
          f2.setSize(100,100);
          f2.setVisible(true);
          f2.add(new Label("NOT FOUND"));
          f2.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent we){
              f2.dispose();
            }
          });
        }
      }
    }
    void createFile()throws IOException{
      try{
      String x="";
      hm.put(str,str+".txt");
      File file=new File(str+".txt");
      BufferedWriter bw=new BufferedWriter(new FileWriter(file));
      x+="Registerd Number: "+str+"\n";
      for(int i=0;i<=3;i++){
        x+=l1[i].getText().toString()+t1[i].getText().toString()+"\n";
      }
      bw.write(x);
      bw.close();
    }
      catch(IOException e){}
    }
    void displayFile()throws IOException{
      try{
      String temp="",x="";
      File file=new File(hm.get(str));
      BufferedReader br=new BufferedReader(new FileReader(file));
        final Frame f3=new Frame();
        f3.setSize(400,400);
        f3.setVisible(true);
        f3.setLayout(new GridBagLayout());
        f3.addWindowListener(new WindowAdapter(){
          public void windowClosing(WindowEvent we){
            f3.dispose();
          }
        });
       for(int i=0;i<=3;i++){
        c.gridx=0;
        c.gridy=i;
        x=br.readLine();
        f3.add(new Label(x),c);
    }
  }
      catch(IOException e){
        System.exit(0);
      }
    }
    void deleteFile(){
      String x;
      str=rno.getText().toString();
      File file=new File(str+".txt");
      file.delete();
      final Frame f4=new Frame();
        f4.setSize(100,100);
        f4.setVisible(true);
        f4.setLayout(new GridBagLayout());
        f4.addWindowListener(new WindowAdapter(){
          public void windowClosing(WindowEvent we){
            f4.dispose();
          }
        });
        f4.add(new Label("FILE DELETED"));
    }
      public static void main(String[] args){
        DataBase d=new DataBase();
      }
}
