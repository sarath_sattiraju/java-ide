import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
class notepad extends Frame implements ActionListener{
	TextArea ta=new TextArea();
	MenuItem newfile=new MenuItem("New");
	MenuItem savefile=new MenuItem("Save");
	MenuItem exit=new MenuItem("Exit");
	TextField tf;
	Frame f;
	Button save;
	notepad(){
		super("NOTEPAD");
		setSize(800,600);
		setVisible(true);
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we){
				System.exit(0);
			}
		});
		MenuBar m=new MenuBar();
		Menu file=new Menu("File");
		setMenuBar(m);
	
		newfile.addActionListener(this);
		savefile.addActionListener(this);
		exit.addActionListener(this);

		file.add(newfile);
		file.add(savefile);
		file.add(exit);
		
		m.add(file);
		add(ta);
					
	}
	public void actionPerformed(ActionEvent ae){
	try{
		if(ae.getSource()==savefile){
			f=new Frame("SAVE");
			f.setSize(400,200);
			f.setVisible(true);
			f.setLayout(new FlowLayout());
			tf=new TextField(15);
			f.add(tf);
			f.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we){
				f.dispose();
			}
		});
			save=new Button("SAVE");
			f.add(save);		
		}
		if(ae.getSource()==save){
			String str=tf.getText().toString();
			File f1=new File(str+".txt");
			BufferedWriter bw=new BufferedWriter(new FileWriter(f1));
			bw.write(str);	
			bw.close();
			f.dispose();
		}
		if(ae.getSource()==newfile){
			ta.setText("");
		}
		if(ae.getSource()==exit){
			System.exit(0);
		}
	}
	catch(IOException e){
	}
}
	public static void main(String[] args){
		notepad n=new notepad();
	} 	
}

