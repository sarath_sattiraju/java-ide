import java.io.*;
import java.util.*;

public class ProcessBuilderDemo { 
    public static void main(String [] args) throws IOException {

        String[] command = {"C:\\Program Files\\Java\\jdk1.8.0_111\\bin","/c","javac MainClass.java"};
        ProcessBuilder probuilder = new ProcessBuilder( command );
        //You can set up your work directory

        Process process = probuilder.start();

        //Read out dir output
        InputStream is = process.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line;
        System.out.printf("Output of running %s is:\n",
                Arrays.toString(command));
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }

        //Wait to get exit value
        // try {
        //     int exitValue = process.waitFor();
        //     System.out.println("\n\nExit Value is " + exitValue);
        // } catch (InterruptedException e) {
        //     // TODO Auto-generated catch block
        //     e.printStackTrace();
        // }
    }
}