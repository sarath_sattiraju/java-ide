import javax.swing.*;
import javax.swing.event.*;
import javax.imageio.ImageIO;
import java.awt.event.*;
import java.io.*;
import java.awt.*;
import java.awt.Font;
import java.util.*;
import javax.swing.filechooser.FileNameExtensionFilter;





class Application extends JFrame implements ActionListener,KeyListener
{

JMenuBar m;
JPanel p,status,lines;
JMenu FILE,EDIT,HELP,RUNORCOMPILE;
JMenuItem NEW,OPEN,SAVE,SAVEAS,EXIT,FONT,RUN,COMPILE,ABOUT;
JTextArea TA,LINE;
JLabel fileName,num;
JFileChooser JC;
JComboBox fontFamily,fontSize;
JRadioButton bold,italic,plain;
int count=1,flag=1,size=12,i=1;
String sizes[]=new String[30];
Font font[];
String fonts[];

JScrollPane scroll;
















////////////////////////////////////////////CONSTRUCTOR////////////////////////////////////////////////////////////

	Application()
	{
	setVisible(true);
	setTitle("Java IDE");
	setSize(1200,800);
	
	setResizable(true);

	ImageIcon newIcon=new ImageIcon("new.png");
	ImageIcon saveIcon=new ImageIcon("save.png");
	ImageIcon saveasIcon=new ImageIcon("save as.png");
	ImageIcon openIcon=new ImageIcon("open.png");
	ImageIcon exitIcon=new ImageIcon("exit.png");
	ImageIcon fontIcon=new ImageIcon("font.png");
	ImageIcon runIcon=new ImageIcon("run.png");
	ImageIcon compileIcon=new ImageIcon("compile.png");
	ImageIcon aboutIcon=new ImageIcon("about.png");
	
	GraphicsEnvironment e = GraphicsEnvironment.getLocalGraphicsEnvironment();
	font=e.getAllFonts();
	fonts=new String[font.length];
	for(int i=0;i<font.length;i++){
		fonts[i]=font[i].getFontName();
	}

	try {
    	setIconImage(ImageIO.read(new File("ide.png")));
	}
	catch (IOException exc) {
    	exc.printStackTrace();
	}

	int k=0;
	for(int i=12;i<=70;i+=2){
		sizes[k]=String.valueOf(i);
		k++;
	}
	k=0;
	
	if(flag==0){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	else{
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		flag=0;
	}



	p=new JPanel();
	JC =new JFileChooser();
	m=new JMenuBar();
	setJMenuBar(m);
	FILE =new JMenu("File");
	m.add(FILE);

	RUNORCOMPILE =new JMenu("Run/Compile");

	NEW=new JMenuItem("New",newIcon);
	NEW.addActionListener(this);
	FILE.add(NEW);

	//KeyBoard Shortcut
	NEW.setMnemonic(KeyEvent.VK_N);
	NEW.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
	

	OPEN =new JMenuItem("Open",openIcon);
	OPEN.addActionListener(this);
	FILE.add(OPEN);

	//KeyBoard Shortcut
	OPEN.setMnemonic(KeyEvent.VK_O);
	OPEN.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
	
	SAVE =new JMenuItem("Save",saveIcon);
	//KeyBoard Shortcut
	SAVE.setMnemonic(KeyEvent.VK_S);
	SAVE.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));

	FILE.add(SAVE);
	SAVE.addActionListener(this);

	SAVEAS=new JMenuItem("Save As",saveasIcon);
	FILE.add(SAVEAS);
	SAVEAS.addActionListener(this);

	//KeyBoard Shortcut
	SAVEAS.setMnemonic(KeyEvent.VK_S);
	SAVEAS.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.SHIFT_MASK));


	EXIT =new JMenuItem("Exit",exitIcon);
	FILE.add(EXIT);
	EXIT.addActionListener(this);

	EDIT=new JMenu("Edit");
	m.add(EDIT);

	FONT=new JMenuItem("Font",fontIcon);
	EDIT.add(FONT);
	FONT.addActionListener(this);

	RUN =new JMenuItem("Run",runIcon);
	COMPILE= new JMenuItem("Compile",compileIcon);
	RUN.addActionListener(this);
	COMPILE.addActionListener(this);


	//KeyBoard Shortcut
	RUN.setMnemonic(KeyEvent.VK_F9);
	RUN.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F9, ActionEvent.CTRL_MASK));

	COMPILE.setMnemonic(KeyEvent.VK_F5);
	COMPILE.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, ActionEvent.CTRL_MASK));

	RUNORCOMPILE.add(RUN);
	RUNORCOMPILE.add(COMPILE);

	m.add(RUNORCOMPILE);

	HELP= new JMenu("Help");
	ABOUT=new JMenuItem("About",aboutIcon);
	ABOUT.addActionListener(this);

	HELP.add(ABOUT);
	m.add(HELP);
	
	lines=new JPanel();
	// full=new JPanel(new FlowLayout(FlowLayout.LEADING));
	TA =new JTextArea();
	// full.add(lines);
	// full.add(TA);
	TA.addKeyListener(this);
	scroll=new JScrollPane(TA);
	p.setLayout(new BorderLayout());
	p.add(scroll,BorderLayout.CENTER);

	lines.setPreferredSize(new Dimension(25,lines.getHeight()));
	lines.setBackground(Color.WHITE);
	LINE=new JTextArea();
	LINE.setText("1");
	lines.add(LINE);
	p.add(lines,BorderLayout.WEST);

	status=new JPanel(new FlowLayout(FlowLayout.LEADING));
	p.add(status,BorderLayout.SOUTH);
	status.setBackground(Color.WHITE);
	status.setPreferredSize(new Dimension(status.getWidth(),30));
	status.revalidate();

	fileName=new JLabel("Untitled *");
	fileName.setForeground(new Color(56,56,56));
	fileName.setFont(new Font("Calibri Light",Font.PLAIN,16));
	status.add(fileName);

	status.add(new JLabel("          "));

	setStatus();
	num.setForeground(new Color(56,56,56));
	status.add(num);

	add(p);



	}




///////////////////////////////////////ACTION PERFORMED//////////////////


	public void actionPerformed(ActionEvent ae)
	{

	/////NEW//
	if(ae.getSource()==NEW)
	{
		new Application();
	}

	////////////////EXIT/////////////////


	if(ae.getSource()==EXIT)
	{
		System.exit(0);
	}

	////////////////OPEN//////////////////////

	if(ae.getSource()==OPEN)
	{
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Text Files", "txt", "text" ,"java");
		int returnVal=JC.showOpenDialog(this);
		count=0;
		if(returnVal==JFileChooser.APPROVE_OPTION)
		{
			File f=JC.getSelectedFile();

			BufferedReader br = null;
			setTitle(""+f.getName()+" *");

		try {

			String sCurrentLine,s="";

			br = new BufferedReader(new FileReader(f));

			while ((sCurrentLine = br.readLine()) != null) {
				s+=sCurrentLine+"\n";
									}
				TA.setText(s);

			} catch (IOException e) {
					e.printStackTrace();
						} 
			fileName.setText(f.getName());			

			int l=TA.getText().toString().length();
			num.setText(String.valueOf(l));


		}
		
	}



////////////////////////SAVE////////////////////



	if(ae.getSource()==SAVE){
		try{
			if(count==1){
			FileNameExtensionFilter filter = new FileNameExtensionFilter("Text Files", "txt", "text" ,"java");
			JC.setFileFilter(filter);
		int selection=JC.showSaveDialog(this);
		if(selection==JC.APPROVE_OPTION){
			File f=JC.getSelectedFile();
			fileName.setText(f.getName());



			
			BufferedWriter bw=new BufferedWriter(new FileWriter(f));
			BufferedReader br=new BufferedReader(new StringReader(TA.getText()));

			String s="";
			s=br.readLine();
			while(s!=null)
			{

				bw.write(s);
				bw.newLine();
				s=br.readLine();
			}


			br.close();
			bw.close();
		}
	}
	else{
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Text Files", "txt", "text" ,"java");
		JC.setFileFilter(filter);
		File f=JC.getSelectedFile();
		try{
			
			BufferedWriter bw=new BufferedWriter(new FileWriter(f));
			BufferedReader br=new BufferedReader(new StringReader(TA.getText()));

			String s="";
			s=br.readLine();
			while(s!=null)
			{

				bw.write(s);
				bw.newLine();
				s=br.readLine();
			}
			
			setTitle(""+f.getName());
			bw.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		fileName.setText(f.getName());
	}
}
	catch(Exception e){
		e.printStackTrace();
	}
}





////////////////////SAVE AS///////////////////////


if(ae.getSource()==SAVEAS){
	try{
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Text Files", "txt", "text" ,"java");
		JC.setFileFilter(filter);
		int selection=JC.showSaveDialog(this);
		if(selection==JC.APPROVE_OPTION){
			File f=JC.getSelectedFile();
			fileName.setText(f.getName());
			setTitle(""+f.getName()+"");
			String written=TA.getText();
			BufferedWriter bw=new BufferedWriter(new FileWriter(f));
			bw.write(written);
			bw.close();
		}
	}
	catch(Exception e){
		e.printStackTrace();
	}

	}



//////////////////////////////RUN AND COMPILE/////////////////////////////////////////
if(ae.getSource()==RUN)
{
	try{ //Process pro = Runtime.getRuntime().exec("");
		
		//C:\Users\mohan\Documents\myjava	
		// ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", "cd C:\\Users\\sarat\\OneDrive\\Documents\\Coursera\\java-ide\\","&&javac C_Compile.java");
  //       builder.redirectErrorStream(true);
  //      Process p =  builder.start();

  //    // p=builder.start();

  //    // p=Runtime.getRuntime().exec("java optimize");
  //      ProcessBuilder builder1 = new ProcessBuilder("cmd.exe", "/c","&&tree");
  //      builder1.redirectErrorStream(true);
  //      Process p1 =  builder1.start();
  
  //   // Process  pm=Runtime.getRuntime().exec("dir");
  //     //  Process  pm2=Runtime.getRuntime().exec("java optimiz");
  //      BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
  //       String line;
  //       while (true) {
  //           line = r.readLine();
  //           if (line == null) { break; }
  //           System.out.println(line);
		String input="";
		Process p = Runtime.getRuntime().exec("javac sample.java");
             Process p2 = Runtime.getRuntime().exec("java sample");

             BufferedReader in = new BufferedReader(  
                                new InputStreamReader(p2.getInputStream()));

             OutputStream out = p.getOutputStream();
             String line = null; 
             line = in.readLine();
             System.out.println(line);
             input=input+"\n";
             out.write(input.getBytes());
             p.wait(10000);
             out.flush();
        }catch (IOException e) {  
             e.printStackTrace();  
        }    
        catch(Exception e){
        	e.printStackTrace();
        }

	}



/////////////////////////////FONT///////////////////////////////
if(ae.getSource()==FONT){
	JFrame f=new JFrame("Font");
		try {
    		f.setIconImage(ImageIO.read(new File("ide.png")));
		}
		catch (IOException exc) {
    		exc.printStackTrace();
		}
	f.setSize(400,200);
	f.setVisible(true);
	f.setResizable(false);
	f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	f.setLayout(new GridBagLayout());
	GridBagConstraints c=new GridBagConstraints();

	JPanel p1=new JPanel();
	p1.setLayout(new FlowLayout());
	c.gridx=0;
	c.gridy=0;
	c.anchor=GridBagConstraints.LINE_START;

	JLabel titleSize=new JLabel("Font Size         ");

	fontSize=new JComboBox(sizes);
	fontSize.setSelectedIndex(0);
	fontSize.addActionListener(this);

	p1.add(titleSize);
	p1.add(fontSize);
	f.add(p1,c);

	JPanel p2=new JPanel();
	p2.setLayout(new FlowLayout());
	c.gridx=0;
	c.gridy=1;
	c.anchor=GridBagConstraints.LINE_START;

	JLabel titleFamily=new JLabel("Font Family      ");

	fontFamily=new JComboBox(fonts);
	fontFamily.setSelectedIndex(0);
	fontFamily.addActionListener(this);

	p2.add(titleFamily);
	p2.add(fontFamily);
	f.add(p2,c);

	JPanel p3=new JPanel();
	p3.setLayout(new FlowLayout());
	c.gridx=0;
	c.gridy=2;
	c.anchor=GridBagConstraints.LINE_START;

	JLabel titleStyle=new JLabel("Font Style      ");

	bold=new JRadioButton("Bold   ");
	bold.addActionListener(this);
	italic=new JRadioButton("Italic   ");
	italic.addActionListener(this);
	plain=new JRadioButton("Plain   ");
	plain.addActionListener(this);
	plain.setSelected(true);
	
	p3.add(titleStyle);
	p3.add(plain);
	p3.add(bold);
	p3.add(italic);
	f.add(p3,c);

}

if(ae.getSource()==fontSize){
	int size=Integer.parseInt((String)fontSize.getSelectedItem());
	TA.setFont(new Font((String)fontFamily.getSelectedItem(),Font.PLAIN,size));
}

if(ae.getSource()==fontFamily){
	int size=Integer.parseInt((String)fontSize.getSelectedItem());
	TA.setFont(new Font((String)fontFamily.getSelectedItem(),Font.PLAIN,size));
}

if(ae.getSource()==bold){
	int size=Integer.parseInt((String)fontSize.getSelectedItem());
	TA.setFont(new Font((String)fontFamily.getSelectedItem(),Font.BOLD,size));	
	italic.setSelected(false);
	plain.setSelected(false);
}

if(ae.getSource()==italic){
	int size=Integer.parseInt((String)fontSize.getSelectedItem());
	TA.setFont(new Font((String)fontFamily.getSelectedItem(),Font.ITALIC,size));
	bold.setSelected(false);
	plain.setSelected(false);	
}

if(ae.getSource()==plain){
	int size=Integer.parseInt((String)fontSize.getSelectedItem());
	TA.setFont(new Font((String)fontFamily.getSelectedItem(),Font.PLAIN,size));	
	italic.setSelected(false);
	bold.setSelected(false);
}

if(ae.getSource()==ABOUT){
	JFrame f=new JFrame("About");
		try {
    		f.setIconImage(ImageIO.read(new File("ide.png")));
		}
		catch (IOException exc) {
    		exc.printStackTrace();
		}
	f.setSize(500,200);
	f.setVisible(true);
	f.setBackground(Color.lightGray);
	f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	f.setResizable(false);
	f.setLayout(new GridBagLayout());
	GridBagConstraints c=new GridBagConstraints();

	c.gridx=0;
	c.gridy=0;

	JPanel p1=new JPanel();
	p1.setLayout(new FlowLayout());

	JLabel title=new JLabel("Java IDE");
	title.setFont(new Font("Arial",Font.PLAIN,36));
	p1.add(title);
	f.add(p1,c);

	JPanel p2=new JPanel();
	p2.setLayout(new FlowLayout());

	c.gridx=0;
	c.gridy=1;
	JLabel content=new JLabel("Copyright \u00a9 2016-2020");
	content.setFont(new Font("Arial",Font.PLAIN,22));
	p2.add(content);
	f.add(p2,c);

	JPanel p3=new JPanel();
	p3.setLayout(new FlowLayout());

	c.gridx=0;
	c.gridy=2;
	JLabel content1=new JLabel("By Mohan Krishna Kosetty, Sarath Sattiraju");
	content.setFont(new Font("Arial",Font.PLAIN,22));
	p3.add(content1);
	f.add(p3,c);

}

}//action perform end brace//

public void setStatus(){
	String str=TA.getText().toString();
	int l=str.length();
	if(l!=0){
		num=new JLabel(String.valueOf(l));
	}else{
		num=new JLabel("");
	}
	num.setFont(new Font("Calibri Light",Font.PLAIN,16));
}

public void keyReleased(KeyEvent e){}

public void keyPressed(KeyEvent e){
	int l=TA.getText().toString().length();

	String str=fileName.getText().toString();
	int l1=str.length();
	char arr[]=new char[l1];
	arr=str.toCharArray();
	if(arr[l1-1]!='*')
	fileName.setText(str+" *");

	num.setText(String.valueOf(l)+" characters typed");

	String line=String.valueOf(++i);
	if(e.getKeyCode()==KeyEvent.VK_ENTER){
		LINE.setText(LINE.getText().toString()+"\n"+line);
	}
}

public void keyTyped(KeyEvent e){}
///////////////////////////////class end/////////////////////////////////////////////////////////
}















/////////////////////////////////////////////MAIN CLASS////////////////////////////////////////////////////////////////////////////////////////////////////









class MainClass
{
	public static void main(String args[])
	{
		try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) { }

	Application a=new Application();
	}
}
	